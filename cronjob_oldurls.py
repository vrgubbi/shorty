import sys, os, django
# append root folder of django project
# could be solved with a relative path like os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..') which corresponds to the parent folder of the actual file.
# if project is running in different env, change path below
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(PROJECT_ROOT + 'shorty/')
print('PROJECT_ROOT: ', PROJECT_ROOT)
# sys.path.append('/Users/ichbin/git_projects/shorty/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shorty.settings")
django.setup()

from django.db.models import Q
from main.models import ShortURL
from datetime import datetime, timedelta

def remove_old_urls():
	'''
	This function runs periodically to deactivate all the stale entries of old and unused urls like:
	case 1) That have not been accessed for a while (longer than 30 days). 
	case 2) The short links, created by authenticated users, will also be removed 
		if they were not used for 60 days and had less than 2 usages.
	'''
	# For anonymous entries
	old_urls_anonymous = ShortURL.objects.filter(Q(accessed_at__lt = datetime.now() - 
		timedelta(days=30)) | Q(accessed_at__isnull = True), created_at__lt = datetime.now() - 
		timedelta(days=30), author__isnull=True, is_active=True)
	for obj in old_urls_anonymous:
		obj.is_active = False
		obj.save()

	# For registered user
	old_urls_users = ShortURL.objects.filter(Q(accessed_at__lt = datetime.now() - 
		timedelta(days=60)) | Q(accessed_at__isnull = True), created_at__lt = datetime.now() - 
		timedelta(days=60), author__isnull=False, is_active=True, times_accessed__lt = 2)
	for obj in old_urls_users:
		obj.is_active = False
		obj.save()
	# print(old_urls_anonymous, 555555)
	# print(old_urls_users, 666666)

if __name__ == '__main__':
	remove_old_urls()
	