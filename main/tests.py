from django.contrib.auth.models import User
from django.test import TestCase

from main.models import ShortURL


class TestCreateDeactivateUrl(TestCase):
    '''
    Following tests uses database which will not use our “real” 
    (production) database. Separate, blank databases are created for the tests.
    Regardless of whether the tests pass or fail, the test databases are destroyed 
    when all the tests have been executed.
    (test classes as subclasses of django.test.TestCase rather than unittest.TestCase)
    $ python manage.py test
    '''
    def test_anonymous_create(self):
        '''
        Tests create url from anonymous user using url '/api/urls/?url=http://google.com/'.
        Table: ShortURL
        '''
        self.assertEqual(0, ShortURL.objects.count())

        response = self.client.post(
            '/api/urls/',
            {'url': 'http://google.com/'},
            content_type='application/json',
            HTTP_HOST='testserver',
        )
        data = response.data

        new_short_url = ShortURL.objects.get()
        self.assertEqual('http://google.com/', data['url'])
        self.assertEqual(f'http://testserver/r/?id={new_short_url.short_id}', data['alias'])
        self.assertIsNone(new_short_url.author)

    def test_create_with_author(self):
        '''
        Tests create url from authenticated user using url '/api/urls/?url=http://google.com/'.
        Table: ShortURL
        '''
        user = User.objects.create_user(username='testuser', password='123456')
        self.client.login(username='testuser', password='123456')

        self.client.post(
            '/api/urls/',
            {'url': 'http://google.com/'},
            content_type='application/json',
            HTTP_HOST='testserver',
        )
        new_short_url = ShortURL.objects.get()
        self.assertEqual(user, new_short_url.author)

    def test_anonymous_url_deactivate(self):
        '''
        Tests Delete request from anonymous user using url '/api/urls/short_id/
        It just sets field is_active to False
        View: ShortURLRemoveAPI
        Table: ShortURL
        '''
        new_short_url = ShortURL.objects.create()
        self.assertEqual(True, new_short_url.is_active)

        response = self.client.delete(
            '/api/urls/' + str(new_short_url.short_id) + '/',
            content_type='application/json',
            HTTP_HOST='testserver',
        )
        updated_short_url = ShortURL.objects.get()
        self.assertEqual(False, updated_short_url.is_active)

    def test_users_url_deactivate(self):
        '''
        Tests Delete request from authenticated user using url '/api/urls/short_id/
        It just sets field is_active to False
        View: ShortURLRemoveAPI
        Table: ShortURL
        '''
        user = User.objects.create_user(username='testuser', password='123456')
        self.client.login(username='testuser', password='123456')
        new_short_url = ShortURL.objects.create(author=user)
        self.assertEqual(True, new_short_url.is_active)
        
        response = self.client.delete(
            '/api/urls/' + str(new_short_url.short_id) + '/',
            content_type='application/json',
            HTTP_HOST='testserver',
        )
        updated_short_url = ShortURL.objects.get()
        self.assertEqual(False, updated_short_url.is_active)

    # def test_redirect_to_url(self):
    #     pass

