from django.urls import path

from . import views

urlpatterns = [
    path('r/', views.redirect_to_url),
    path('api/urls/', views.ShortURLListCreateAPI.as_view()),
    path('api/urls/<short_id>/', views.ShortURLRemoveAPI.as_view()),
]