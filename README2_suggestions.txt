Security:
1) Allowed hosts section is allowing every url, we can change it to only whichever needed 
example: localhost, <site url>
2) Debug should be made False when deployed in production, it should be true only while development.


Requirements.txt:
Perfect, no changes needed, we can add new libraries to this file as and when needed.


Database:
Like you mentioned in the document, I'll choose sqlite3 as a database for this project which is simple to use for small application requirement. We can switch to MySQL or Postgresql if we want to handle bigger and more complex data.


Migrations:
Since one more field is added in models.py, apply migrations for the app 'main' before starting the project.
$ python manage.py migrate main


Corrections:
1) There was error in 'created_at' field, as that field was changing its value everytime when that url is accessed, instead changes has been made to add creation datetime only when first time that object is created.
'auto_now_add' field is added and removed manual adding of created_at date from the custom function 'save()'

2) packed urls within its respective app.
Shifted urls belonging to main app urls.py, from project level urls.py


Feature Development:
1) Created a file cronjob_oldurls.py in the project directory which will then be used as a cronjob file to run periodically to do the following jobs.
remove stale data from DB, 
- case 1) That have not been accessed for a while (longer than 30 days). created by anonymous users.
- case 2) Should remove stale data if they were not used for 60 days and had less than 2 usages . created by authenticated users.
Syntax:
$ crontab -e
# cronjob for running python script everyday at 1
0 1 * * * /path/to/project/cronjob_oldurls.py

2) Added new field in the table 'is_active'.
whenever we hit the delete request of old urls, is_active value will be set to False instead of deleting the entry from the ShortURL table.
by this way short ids of deleted ShortURLs will not ever be reused when we create new ShortURLs.


Comments:
Added comments and docs whereever necessary in the code.


Tests:
Added Test cases in two modules.
- one for app functionality 'app/test.py'
- second for testing cronjob 'test_cron_removeurl.py'
Added testcases to check all the new features functionality.

Testcase result:
	$ python manage.py test
	PROJECT_ROOT:  /Users/ichbin/git_projects/shorty
	Creating test database for alias 'default'...
	System check identified no issues (0 silenced).
	...........
	----------------------------------------------------------------------
	Ran 11 tests in 1.444s

	OK
	Destroying test database for alias 'default'...
