# Generated by Django 3.1 on 2020-08-10 19:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shorturl',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
