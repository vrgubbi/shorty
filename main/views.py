from datetime import datetime

from django.http import Http404
from django.shortcuts import redirect
from rest_framework.generics import (
    ListCreateAPIView, DestroyAPIView,
    RetrieveDestroyAPIView,
)

from main.models import ShortURL
from main.serializers import ShortURLSerializer


def redirect_to_url(request):
    '''
    Upon GET request, this function returns and redirects to Full url for id(short_id)
    Parameter: id - 'short_id' field from table 'ShortURL'
    Response: redirects to url of 'url' field from table 'ShortURL'
    '''
    try:
        short_url = ShortURL.objects.get(short_id=request.GET['id'], is_active=True)
    except ShortURL.DoesNotExist:
        raise Exception

    short_url.accessed_at = datetime.now()
    short_url.times_accessed = short_url.times_accessed + 1
    short_url.save()

    return redirect(short_url.url)


class ShortURLListCreateAPI(ListCreateAPIView):
    '''
    This class is used add new entries to the table 'ShortURL'
    '''
    serializer_class = ShortURLSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return ShortURL.objects.filter(author=self.request.user, is_active=True)
        else:
            return ShortURL.objects.none()


class ShortURLRemoveAPI(RetrieveDestroyAPIView):
    '''
    This class is used deactive the selected url entry from the table 'ShortURL'
    '''
    lookup_field = 'short_id'
    serializer_class = ShortURLSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return ShortURL.objects.filter(author=self.request.user, is_active=True)
        else:
            return ShortURL.objects.filter(author__isnull=True, is_active=True)

    def perform_destroy(self, instance):
        '''
        RetrieveDestroyAPIView has a perform_destroy method. 
        we can override that and added custom logic of making is_active field False.
        '''
        instance.is_active = False
        instance.save()
