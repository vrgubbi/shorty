from django.test import TestCase
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from main.models import ShortURL
from cronjob_oldurls import remove_old_urls

class TestCronStaleURL(TestCase):
    '''
    Following tests uses database which will not use our “real” 
    (production) database. Separate, blank databases are created for the tests.
    Regardless of whether the tests pass or fail, the test databases are destroyed 
    when all the tests have been executed.
    (test classes as subclasses of django.test.TestCase rather than unittest.TestCase)
    $ python manage.py test
    '''
    def test_remove_old_urls_anonymous_100days(self):
        '''
        Tests function remove_old_urls(), which should deactivate old entries for anonymous user
        remove all old entries less than 100 days.
        No Author
        days old: 100 days
        is_active should be False
        Table: ShortURL
        '''
        new_short_url = ShortURL.objects.create()
        new_short_url.created_at -= timedelta(days = 100)
        new_short_url.save()
        self.assertEqual(True, new_short_url.is_active)

        # calling function remove_old_urls should deactivate old entries
        remove_old_urls()

        updated_short_url = ShortURL.objects.get(short_id=new_short_url.short_id)
        # Now it should become is_active=False for 100 days old record
        self.assertEqual(False, updated_short_url.is_active)


    def test_remove_old_urls_anonymous_5days(self):
        '''
        Tests function remove_old_urls(), which should deactivate old entries for anonymous user
        No Author
        days old: 5 days
        is_active should be True
        Table: ShortURL
        '''
        new_short_url = ShortURL.objects.create()
        new_short_url.created_at -= timedelta(days = 5)
        new_short_url.save()
        self.assertEqual(True, new_short_url.is_active)

        # calling function remove_old_urls should deactivate old entries
        remove_old_urls()

        updated_short_url = ShortURL.objects.get(short_id=new_short_url.short_id)
        # Now it should still be as is_active=True for 5 days old record
        self.assertEqual(True, new_short_url.is_active)


    def test_remove_old_urls_anonymous_100days_10times(self):
        '''
        No Author
        days old: 100 days
        times accessed: 10
        is_active should be False
        '''
        new_short_url = ShortURL.objects.create()
        new_short_url.created_at -= timedelta(days = 100)
        new_short_url.times_accessed = 10
        new_short_url.save()
        self.assertEqual(True, new_short_url.is_active)

        # calling function remove_old_urls should deactivate old entries
        remove_old_urls()

        updated_short_url = ShortURL.objects.get(short_id=new_short_url.short_id)
        # Now it should become is_active=False
        self.assertEqual(False, updated_short_url.is_active)


    def test_remove_old_urls_author_100days(self):
        '''
        Authenticated user
        days old: 100 days
        is_active should be False
        '''
        user = User.objects.create_user(username='testuser', password='123456')
        # self.client.login(username='testuser', password='123456')

        new_short_url = ShortURL.objects.create()
        new_short_url.author = user
        new_short_url.created_at -= timedelta(days = 100)
        new_short_url.save()
        self.assertEqual(True, new_short_url.is_active)

        # calling function remove_old_urls should deactivate old entries
        remove_old_urls()

        updated_short_url = ShortURL.objects.get(short_id=new_short_url.short_id)
        # Now it should become is_active=False
        self.assertEqual(False, updated_short_url.is_active)


    def test_remove_old_urls_author_5days(self):
        '''
        Authenticated user
        days old: 5 days
        is_active should be True
        '''
        user = User.objects.create_user(username='testuser', password='123456')

        new_short_url = ShortURL.objects.create()
        new_short_url.author = user
        new_short_url.created_at -= timedelta(days = 5)
        new_short_url.save()
        self.assertEqual(True, new_short_url.is_active)

        updated_short_url = ShortURL.objects.get(short_id=new_short_url.short_id)
        # calling function remove_old_urls should deactivate old entries
        remove_old_urls()

        # Now it should become is_active=True
        self.assertEqual(True, updated_short_url.is_active)


    def test_remove_old_urls_author_100days_10times(self):
        '''
        Authenticated user
        days old: 100 days
        is_active should be True
        '''
        user = User.objects.create_user(username='testuser', password='123456')

        new_short_url = ShortURL.objects.create()
        new_short_url.author = user
        new_short_url.created_at -= timedelta(days = 100)
        new_short_url.times_accessed = 10
        new_short_url.save()
        self.assertEqual(True, new_short_url.is_active)

        # calling function remove_old_urls should deactivate old entries
        remove_old_urls()

        updated_short_url = ShortURL.objects.get(short_id=new_short_url.short_id)
        # Now it should become is_active=True
        self.assertEqual(True, updated_short_url.is_active)


    def test_remove_old_urls_author_100days_1time(self):
        '''
        Authenticated user
        days old: 100 days
        is_active should be False
        '''
        user = User.objects.create_user(username='testuser', password='123456')

        new_short_url = ShortURL.objects.create()
        new_short_url.author = user
        new_short_url.created_at -= timedelta(days = 100)
        new_short_url.times_accessed = 1
        new_short_url.save()
        self.assertEqual(True, new_short_url.is_active)

        # calling function remove_old_urls should deactivate old entries
        remove_old_urls()

        updated_short_url = ShortURL.objects.get(short_id=new_short_url.short_id)
        # Now it should become is_active=False
        self.assertEqual(False, updated_short_url.is_active)

